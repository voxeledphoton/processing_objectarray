
float pi=3.14159265358979;
int unit = 80;
int count;
tri[] triangles;
float spa=15;
void setup(){
  size(1920,1000);
  background(30);
  int wideCount = width / unit;
  int highCount = height / unit;
  count = wideCount * highCount;
  triangles = new tri[count];
  
  int index = 0;
  for (int yy = 0; yy < highCount; yy++) {
    for (int xx = 0; xx < wideCount; xx++) {
      triangles[index++] = new tri(unit,xx*unit,yy*unit,10,10,10,10,11*xx+yy);
    }
  }
}
void draw(){
  fill(0,30);
  noStroke();
  rect(0,0,width,height,.1);
  noFill();
  stroke(255);//(frameCount*.1)%255,(frameCount*.4)%255,frameCount%255);
  strokeWeight(2);
  for (tri triangles : triangles) {
    triangles.calculate();
    triangles.display();
  }
}
class tri
{
  private float i;
  private float x;
  private float y;
  private float lx;
  private float ly;
  private float llx;
  private float lly;
  private float newdir;
  private float adir;
  tri(float ii, float ix, float iy, float ilx, float ily, float illx, float illy, float iadir){
    i=ii;
    x=ix+spa*2;
    y=iy+spa*2;
    lx=ilx;
    ly=ily;
    llx=illx;
    lly=illy;
    newdir=0;
    adir=iadir;
  }
  void calculate(){
    //x=spa*2+i*spa*4;
    //y=spa*2;
    llx=lx;
    lly=ly;
    newdir+=adir-mouseX;
    lx=x+lengthdir_x(spa,newdir)+sin(frameCount)*spa;
    ly=y+lengthdir_y(spa,newdir)+cos(frameCount)*spa;
    
  }
  void display(){
    //bezier(x,y,lx,ly,llx,lly,x,y);
    //stroke(x*.1+y*.1+frameCount);
    line(llx,lly,lx,ly);
  }
}
float lengthdir_x(float len, float dir){
  return sin((pi/180)*dir)*len;
}
float lengthdir_y(float len, float dir){
  return cos((pi/180)*dir)*len;  
}